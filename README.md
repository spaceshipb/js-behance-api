# Dealer Inspire Front End Code Challenge

Hi.

## To run
`yarn install` or `npm install`
then
`yarn start` or `npm run start`

I used create-react-app.
`yarn test` or `npm run test` will kick off the test watcher.

I use an expressjs API for CORS, caching, and security purposes.  I left my behance api_key in the npm script (normally wouldn't do that) for convenience.  You can start the API with a different key with `env BEHANCE_API_KEY=yourkey node api/server.js`

## Questions
Let me know if you have any.