var app = require('express')();
var cache = require('memory-cache');

var searchUsers = require('./lib/behance-service').searchUsers;
var getUserDetails = require('./lib/behance-service').getUserDetails;
const PORT = 5005;
const CACHE_DURATION = undefined; //keep forever for now 

app.get('/api/search', function (req, res) {
      var querystring = req.query.q ? `q=${req.query.q.trim()}` : `country=${req.query.country.trim()}`
      var cachedUsers = cache.get(querystring);
      if(cachedUsers) {
        res.send(cachedUsers);
        return;
      } 

      searchUsers(querystring).then(result => {
          var users = result.users || [];
          cache.put(querystring, users, CACHE_DURATION);
          res.send(users);
      });
  });

  app.get('/api/users/:userId/:endpoint?', function (req, res) {
    var userId = req.params.userId;
    var endpoint = req.params.endpoint || '';
    var userCacheKey = `user_${userId}_${endpoint}`;
    var cachedUser = cache.get(userCacheKey);
    if(cachedUser) {
      res.send(cachedUser);
      return;
    } 

    getUserDetails(userId, endpoint).then(result => {
        var user = result[endpoint || 'user'] || {};
        cache.put(userCacheKey, user, CACHE_DURATION);
        res.send(user);
    })
    .catch(err => {
      res.status(500).send('Unable to get user details!')
    });
});


  app.listen(PORT, () => console.log(`listening on port ${PORT}!`))