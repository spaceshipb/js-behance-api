var get = require('./data').get;

const apiUrl = "http://behance.net";
const apiKey = process.env.BEHANCE_API_KEY;

function buildApiUrl(url, query, key) {
    return `${url}/v2/users?${query}&api_key=${key}`;
}

function buildUserDetailsApiUrl(url, userId, endpoint, key) {
    const endpointSuffix = endpoint ? `/${endpoint}` : '';
    return `${url}/v2/users/${userId}${endpointSuffix}?api_key=${key}`;
}

function searchUsers(querystring) {
    return get(buildApiUrl(apiUrl, querystring, apiKey))
        .then(function(result) {
            return result;
        });
    
}

function getUserDetails(userId, endpoint) {
    var userDetailsApi = buildUserDetailsApiUrl(apiUrl, userId, endpoint, apiKey);
    return get(userDetailsApi)
        .then(function(result) {
            return result;
        });
    
}

module.exports = {
    searchUsers: searchUsers,
    getUserDetails: getUserDetails,
}

