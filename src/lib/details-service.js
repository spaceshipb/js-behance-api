import { get } from './data';
import { API_URL, DEFAULT_PROFILE_IMG_URL } from './constants';

export function buildBehanceUserProfileItem(item) {
    return {
        id: item.id,
        displayName: item.display_name,
        profileImageUrl: item.images['276'] || DEFAULT_PROFILE_IMG_URL,
        location: item.location,
        stats: item.stats,
        fields: item.fields,
    };
}

export function buildApiUrl(url, userId, endpoint) {
    const endpointSuffix = endpoint ? `/${endpoint}` : '';
    return `${url}/users/${userId}${endpointSuffix}`;
}

export const getUserDetails = async (userId) => {
    const result = await get(buildApiUrl(API_URL, userId));
    return buildBehanceUserProfileItem(result);
}

export const getUserProjects = async (userId) => {
    const result = await get(buildApiUrl(API_URL, userId, 'projects'));
    return result;
}

export const getUserWorkExperience = async (userId) => {
    const result = await get(buildApiUrl(API_URL, userId, 'work_experience'));
    return result;
}

export const getUserFollowers = async (userId) => {
    const result = await get(buildApiUrl(API_URL, userId, 'followers'));
    return result;
}

export const getUserFollowing = async (userId) => {
    const result = await get(buildApiUrl(API_URL, userId, 'following'));
    return result;
}
