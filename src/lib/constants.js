// -- SEARCH SETTINGS --
export const SEARCH_WAIT = 600;
export const SEARCH_CHARACTER_MINIMUM = 2;

export const API_URL = '/api';
export const DEFAULT_PROFILE_IMG_URL = '/images/portrait_placeholder.png';
