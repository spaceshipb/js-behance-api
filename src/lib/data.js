import fetch from 'cross-fetch';
require('es6-promise').polyfill();

function handleJsonResponse(response) {
    if (response.status === 503) {
        throw new Error('Api is unavailable');
    }
    if (response.status > 400) {
        throw new Error('Bad response from server');
    }
    if (response.status === 204) {
        return { success: true };
    }
    return response.text()
	    .then((text) => text.length ? JSON.parse(text) : {});

}

export const get = (url) => {
   return fetch(url, {
         method: 'GET', 
      })
        .then(handleJsonResponse);
};
