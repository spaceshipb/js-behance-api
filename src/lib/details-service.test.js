import { buildApiUrl } from './details-service';

it('builds a proper user details endpoint api url', () => {
    const target = buildApiUrl('/api', 1234, 'projects');
    expect(target).toBe('/api/users/1234/projects');
});

it('builds a proper user details api url', () => {
    const target = buildApiUrl('/api', 1234);
    expect(target).toBe('/api/users/1234');
});
