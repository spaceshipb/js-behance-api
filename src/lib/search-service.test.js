import { buildBehanceUserListItem, buildApiUrl } from './search-service';

it('transforms behance user result', () => {
    const user = {
        "id": 1691287,
        "username": "edouardmolinari",
        "images": {
            "50": "https://mir-s3-cdn-cf.behance.net/user/50/0338ff1691287.59a4967dafc24.jpg",
            "100": "https://mir-s3-cdn-cf.behance.net/user/100/0338ff1691287.59a4967dafc24.jpg",
            "115": "https://mir-s3-cdn-cf.behance.net/user/115/0338ff1691287.59a4967dafc24.jpg",
            "230": "https://mir-s3-cdn-cf.behance.net/user/230/0338ff1691287.59a4967dafc24.jpg",
            "138": "https://mir-s3-cdn-cf.behance.net/user/138/0338ff1691287.59a4967dafc24.jpg",
            "276": "https://mir-s3-cdn-cf.behance.net/user/276/0338ff1691287.59a4967dafc24.jpg"
        },
        "display_name": "Edouard Molinari",
    };

    const target = buildBehanceUserListItem(user);
    expect(target.displayName).toBe(user.display_name);
    expect(target.id).toBe(user.id);
    expect(target.thumbImageUrl).toBe(user.images['100']);
});

it('adds a default profile image if no image exists', () => {
    const user = {
        "id": 1691287,
        "username": "edouardmolinari",
        "images": {
        },
        "display_name": "Edouard Molinari",
    };

    const target = buildBehanceUserListItem(user);
    expect(target.thumbImageUrl.length).toBeGreaterThan(1);
});

it('builds a proper api url', () => {
    const target = buildApiUrl('/api', 'q=test');
    expect(target).toBe('/api/search?q=test');
});