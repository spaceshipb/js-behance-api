import { get } from './data';
import { API_URL, DEFAULT_PROFILE_IMG_URL } from './constants';

export function buildApiUrl(url, query) {
    return `${url}/search?${query}`;
}

export function buildBehanceUserListItem(item) {
    return {
        id: item.id,
        displayName: item.display_name,
        thumbImageUrl: item.images['100'] || DEFAULT_PROFILE_IMG_URL,
        username: item.username,
    };
}

export const searchUsers = async (querystring) => {
    const result = await get(buildApiUrl(API_URL, querystring));
    return result.map(buildBehanceUserListItem);
}
