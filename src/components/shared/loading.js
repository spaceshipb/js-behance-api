import React from 'react'

const Loading = () => {
    return (<div className="loading"><img src="/images/loader.svg" alt="loading" /></div>);
}

export default Loading;
