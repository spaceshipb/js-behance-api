import React from 'react';
import Portal from '../../portal';
import Stats from './stats';
import ProjectList from './projects';
import WorkExperience from './work-experience';
import FollowersList from './followers';
import FollowingList from './following';
import Loading from '../shared/loading';

import './user-details.css';

const UserDetails = (props) => {
    const user = props.user;
    if(user.isLoading) {
        return (
            <Portal>
            <div className="overlay"></div>
            <div className="user-details-container">
                <Loading />
            </div>
            </Portal>
        );
    }

    return (
    <Portal>
        <div className="overlay"></div>
        <div className="user-details-container">
            <div className="content">
                <div className="header">
                    <div>
                        <h1>{user.displayName} </h1>
                    </div>
                    <div>
                        <button onClick={props.close}>Close</button>
                    </div>
                </div>
                <section>
                    <div>
                        <h2>{user.location}</h2>
                        <img src={user.profileImageUrl} alt={user.displayName} /> 
                    </div>
                    <ProjectList userId={user.id} />
                    <WorkExperience userId={user.id} />
                </section>
                <Stats stats={user.stats} />
                <FollowersList userId={user.id} launch={props.launch} />
                <FollowingList userId={user.id} launch={props.launch}  />
                
            </div>
        </div>
    </Portal>
    );
}

export default UserDetails;
