import React, { Component } from 'react';
import ProjectItem from './project-item';
import { getUserProjects } from '../../lib/details-service';

class ProjectList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            projects: [],
         };
    }

    componentDidMount() {
        getUserProjects(this.props.userId).then((results) => {
            this.setState({
                projects: results,
            });
        });

    }

    render() { 
        const projects = this.state.projects.map((project) => {
            return (<ProjectItem key={`projectItem${project.id}`} project={project} />);
        });
        return ( <div>
            <h2>Projects</h2>
            <ul className="project-list">
            {projects}
            </ul>
        </div> );
    }
}
 
export default ProjectList;
