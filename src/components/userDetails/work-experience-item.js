import React from 'react';

const WorkExperienceItem = (props) => {
    const experience = props.experience;
    return (
        <li><strong>{experience.organization} - {experience.position}</strong><br />
        {experience.location} <span className="date">{experience.start_date}</span>
        </li>
    );
};

export default WorkExperienceItem; 
