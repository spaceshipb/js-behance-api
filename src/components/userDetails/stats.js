import React from 'react';

const Stats = (props) => {
    const statList = Object.keys(props.stats).reduce((stats, key) => {
        if(props.stats[key] === false) { return stats; }
        stats.push(<li key={`state${key}`}>{key}: {props.stats[key]}</li>);
        return stats;
    }, []);
    return (
        <div className="user-stats">
            <h2>Stats</h2>
            <ul className="stats-list">
            {statList}
            </ul>
        </div>
    );
};

export default Stats;
