import React, { Component } from 'react';
import UserListItem from '../userList/user-list-item';
import { getUserFollowers } from '../../lib/details-service';
import { buildBehanceUserListItem } from '../../lib/search-service';

class FollowersList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            followers: [],
         };
    }

    componentDidMount() {
        getUserFollowers(this.props.userId).then((results) => {
            this.setState({
                followers: results.map(buildBehanceUserListItem),
            });
        });

    }

    render() { 
        const followers = this.state.followers.map((follower) => {
            follower.id = follower.username;
            return (<UserListItem key={`followerItem${follower.id}`} user={follower} launchDetailView={this.props.launch} />);
        });
        return ( <div>
            <h2>Followers</h2>
            <ul className="user-list">
            {followers}
            </ul>
        </div> );
    }
}
 
export default FollowersList;
