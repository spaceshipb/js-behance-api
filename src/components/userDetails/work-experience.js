import React, { Component } from 'react';
import WorkExperienceItem from './work-experience-item';
import { getUserWorkExperience } from '../../lib/details-service';

class WorkExperience extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            experiences: [],
         };
    }

    componentDidMount() {
        getUserWorkExperience(this.props.userId)
            .then((results) => {
                this.setState({
                    experiences: results,
                });
            })
            .catch(() => {});
    }

    render() { 
        const experiences = this.state.experiences.length === 0
                ? 'Not Available'
                : this.state.experiences.map((experience, i) => {
                        return (
                            <WorkExperienceItem
                                key={`experienceItem${i}`}
                                experience={experience} 
                            />
                        );
                    });
        return ( <div>
            <h2>Work Experience</h2>
            <ul>
            {experiences}
            </ul>
        </div> );
    }
}
 
export default WorkExperience;
