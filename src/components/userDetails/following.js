import React, { Component } from 'react';
import UserListItem from '../userList/user-list-item';
import { getUserFollowing } from '../../lib/details-service';
import { buildBehanceUserListItem } from '../../lib/search-service';

class FollowingList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            followers: [],
         };
    }

    componentDidMount() {
        getUserFollowing(this.props.userId).then((results) => {
            this.setState({
                followers: results.map(buildBehanceUserListItem),
            });
        });

    }

    render() { 
        const followers = this.state.followers.map((follower) => {
            follower.id = follower.username;
            return (<UserListItem key={`followingItem${follower.id}`} user={follower} launchDetailView={this.props.launch}  />);
        });
        return ( <div>
            <h2>Following</h2>
            <ul className="user-list">
            {followers}
            </ul>
        </div> );
    }
}
 
export default FollowingList;
