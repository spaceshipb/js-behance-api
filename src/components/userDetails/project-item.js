import React from 'react';

const ProjectItem = (props) => {
    const project = props.project;
    return (
        <li><a href={project.url} target="_blank">{project.name}</a></li>
    );
};

export default ProjectItem; 
