import React, { Component } from 'react';
import UserListItem from './user-list-item';
import './user-list.css';

class UserList extends Component {
    render() { 
        const users = this.props.userList.map(u => {
            return <UserListItem user={u} key={`userItem${u.id}`} launchDetailView={this.props.launchDetailView} />
        });
        return ( <div>
            {this.props.hasSearch && <div className="results-count">Results: {this.props.userList.length}</div>}
            <ul className="user-list">
            {users}
            </ul>
        </div> );
    }
}
 
export default UserList;
