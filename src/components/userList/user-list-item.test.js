import React from 'react';
import ReactDOM from 'react-dom';
import UserListItem from './user-list-item';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<UserListItem user={{}} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
