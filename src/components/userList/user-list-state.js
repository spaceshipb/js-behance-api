import React, { Component } from 'react';

const UserResultState = (WrappedComponent) => {
    return class extends Component {
        constructor() {
            super();

            this.state = {
                userResults: [],
            };
        }

        setUserResults = (userList) => {
            this.setState({
                userResults: userList,
            });
        }

        render() { 
            return (<WrappedComponent 
                            userResults={this.state.userResults}
                            setUserResults={this.setUserResults}
                        />);
        }
    }
};

export default UserResultState;
