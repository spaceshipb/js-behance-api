import React, { Component } from 'react';

class UserListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }

    showDetails = () => {
        this.props.launchDetailView(this.props.user.id);
    }

    render() { 
        const user = this.props.user;
        return ( <li className="user-list-item" onClick={this.showDetails}>
            <img src={user.thumbImageUrl} alt={user.displayName} /> 
            <span className="display-name">{user.displayName}</span>
        </li> );
    }
}
 
export default UserListItem;
