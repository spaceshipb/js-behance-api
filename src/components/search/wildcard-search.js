import React, { Component } from 'react';

class WilcardSearch extends Component {
    updateField = (e) => {
        this.props.updateSearch(`q=${e.currentTarget.value}`);
    }
    render() { 
        return (
            <div className="search-container">
                <label className="label" htmlFor="wildsearch">Search</label>
                <input className="form-input" onChange={this.updateField} type="text" name="wildsearch" />
            </div>
        );
    }
}
 
export default WilcardSearch;
