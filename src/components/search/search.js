import React, { Component } from 'react';
import { SEARCH_WAIT, SEARCH_CHARACTER_MINIMUM } from '../../lib/constants';
import './search.css';

const Search = (WrappedComponent) => {
    return class extends Component {
        updateSearch = (querystring) => {
            const search = querystring.split('=');
            const term = search.length > 1 ? search[1] : '';
            if (term.length < SEARCH_CHARACTER_MINIMUM && term.length !== 0) {
                return;
            }

            if (!!this.searchTimeout) {
                window.clearTimeout(this.searchTimeout);
            }

            this.searchTimeout = window.setTimeout(() => {
                this.props.search(querystring);
            }, SEARCH_WAIT);
        }

        render() { 
            return ( 
                <WrappedComponent updateSearch={this.updateSearch} />
             );
        }
    }
}
 
export default Search;
