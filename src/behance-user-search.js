import React, { Component, Fragment } from 'react';
import { searchUsers } from './lib/search-service';
import { getUserDetails } from './lib/details-service';
import Search from './components/search/search';
import WildcardSearch from './components/search/wildcard-search';
import CountrySearch from './components/search/country-search';
import UserList from './components/userList/user-list';
import UserDetails from './components/userDetails/user-details';
import Loading from './components/shared/loading';

const SearchComponent = Search(WildcardSearch);
const CountrySearchComponent = Search(CountrySearch);

const SEARCH_COMPONENT_TYPES = {
  wildcard: 0,
  country: 1,
};

class BehanceUserSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSearch: '',
      userList: [],
      currentUser: null,
      searchType: SEARCH_COMPONENT_TYPES.wildcard,
      isSearching: false,
    };
  }

  clearResults = () => {
    this.setState({
      userList: [],
      currentSearch: '',
      currentUser: null,
      isSearching: false,
    });
  };

  performUserSearch = async querystring => {
    await new Promise((resolve, reject) => {
      this.setState(
        {
          isSearching: true,
          userList: [],
        },
        () => {
          resolve();
        }
      );
    });
    const search = querystring.split('=');
    if (search.length === 2 && search[1] === '') {
      this.clearResults();
      return;
    }

    const userList = await searchUsers(querystring).catch(err => {
      alert(err);
    });
    this.setState({
      userList,
      currentSearch: querystring,
      isSearching: false,
    });
  };

  launchDetailView = userId => {
    this.setState(
      {
        currentUser: { isLoading: true },
      },
      async () => {
        const user = await getUserDetails(userId).catch(err => {
          alert(err);
        });

        this.setState({
          currentUser: user || null,
        });
      }
    );
  };

  closeDetailView = () => {
    this.setState({
      currentUser: null,
    });
  };

  switchComponent = e => {
    e.preventDefault();
    this.setState(({ searchType }) => {
      return {
        searchType:
          searchType === SEARCH_COMPONENT_TYPES.country
            ? SEARCH_COMPONENT_TYPES.wildcard
            : SEARCH_COMPONENT_TYPES.country,
        userList: [],
        currentSearch: '',
      };
    });
  };

  render() {
    const Search =
      this.state.searchType === SEARCH_COMPONENT_TYPES.wildcard
        ? SearchComponent
        : CountrySearchComponent;
    return (
      <Fragment>
        <Search search={this.performUserSearch} />
        <button className="switch-search" onClick={this.switchComponent}>
          {this.state.searchType === SEARCH_COMPONENT_TYPES.wildcard
            ? 'Or Search By Country'
            : 'Free Search'}
        </button>
        {this.state.isSearching && <Loading />}
        <UserList
          userList={this.state.userList}
          hasSearch={this.state.currentSearch.length > 0}
          launchDetailView={this.launchDetailView}
        />
        {this.state.currentUser && (
          <UserDetails
            user={this.state.currentUser}
            close={this.closeDetailView}
            launch={this.launchDetailView}
          />
        )}
      </Fragment>
    );
  }
}

export default BehanceUserSearch;
