import React, { Component } from 'react';
import './styles/app.css';

import BehanceUserSearch from './behance-user-search';

class App extends Component {  
  render() {
    return (
      <div className="main-container">
        <BehanceUserSearch />
      </div>
    );
  }
}

export default App;
